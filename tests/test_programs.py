# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestPrograms(CompilerTestCase):

    def test_recursive_find(self):
        self._test_program(
            """
            int find(int elem, int* string) {
                return _find(elem, string, 0);
            }

            int _find(int elem, int* string, int offset) {
                if (string[offset] == elem) {
                    return offset;
                } else if (string[offset] == 0) {
                    throw NotFound(elem);
                } else {
                    return _find(elem, string, offset+1);
                }
            }


            int main(int argc, int** argv) {
                int* msg;

                msg = malloc(14*8);
                msg[0] = 72; msg[1] = 101; msg[2] = 108; msg[3] = 108;
                msg[4] = 111; msg[5] = 44; msg[6] = 32; msg[7] = 119;
                msg[8] = 111; msg[9] = 114; msg[10] = 108; msg[11] = 100;
                msg[12] = 33; msg[13] = 0;

                printf("%i:", find(72, msg));
                printf("%i:", find(101, msg));
                printf("%i:", find(119, msg));
                try {find(122, msg);} catch (NotFound x) {fprintf(stderr, "Not found: %c\\n", 122);}
                fflush(stdout);

                return 0;
            }
            """,
            Call([], 0, b"0:1:7:", b"Not found: z\n"),
        )
