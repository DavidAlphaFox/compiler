# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestScope(CompilerTestCase):

    def test_nested_definition(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 0;

                {
                    int i;
                    i = 1;
                    printf("%i", i);
                }

                return i;
            }
            """,
            Call([], 0, b"1", b""),
        )

    def test_nested_variables(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 0;

                {
                    i = 1;
                }

                return i;
            }
            """,
            Call([], 1, b"", b""),
        )
