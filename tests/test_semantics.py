# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from . import config
from .base import CompilerTestCase, Call

class TestSemantics(CompilerTestCase):

    def test_function_calls(self):
        # function call arguments are evaluated
        # from right to left
        self._test_program(
            """
            int main() {
                int i;
                i = 0;
                printf("%d%d%d%d%d%d%d%d%d",
                       i++, i++, i++, i++, i++,
                       i++, i++, i++, i++);
                return 0;
            }
            """,
            Call([], 0, b"876543210", b""),
        )

    def test_set_array(self):
        # when setting array, value is computed before key
        self._test_program(
            """
            int main() {
                int array;
                int i;

                array = malloc(8*2);
                array[0] = 0; array[1] = 0;

                i = 0;
                array[i++] = ++i;

                printf("[%d; %d]", array[0], array[1]);
                return 0;
            }
            """,
            Call([], 0, b"[0; 1]", b"")
        )

    def test_order_add(self):
        # right is evaluated before left
        self._test_program(
            """
            int main() {
                int i;
                i = 2;
                return (i++) + i;
            }
            """,
            Call([], 4, b"", b"")
        )
        self._test_program(
            """
            int main() {
                int i;
                i = 2;
                return i + (++i);
            }
            """,
            Call([], 6, b"", b"")
        )

    def test_order_min(self):
        # right is evaluated before left
        self._test_program(
            """
            int main() {
                int i;
                i = 2;
                return (++i) - i;
            }
            """,
            Call([], 1, b"", b"")
        )

    def test_order_mul(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 1;
                return i * (i++);
            }
            """,
            Call([], 2, b"", b"")
        )

    def test_order_div(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 10;
                return i / (i--);
            }
            """,
            Call([], 0, b"", b"")
        )

    def test_order_mod(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 10;
                return i % (i--);
            }
            """,
            Call([], 9, b"", b"")
        )

    def test_order_s_index(self):
        self._test_program(
            """
            int main() {
                int t1, t2, i;


                i = 0;

                t1 = malloc(8*2);
                t2 = malloc(8*2);

                t1[0] = 1; t1[1] = 2;
                t2[0] = 3; t2[1] = 4;

                /* right to left: 3 */
                /* left to right: 4 */
                return (++i?t2:t1)[i];
            }

            """,
            Call([], 3, b"", b"")
        )

    def test_order_lower(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 1;
                return i++<i;
            }
            """,
            Call([], 0, b"", b"")
        )
        self._test_program(
            """
            int main() {
                int i;
                i = 1;
                return i < i--;
            }
            """,
            Call([], 1, b"", b"")
        )

    def test_order_equal(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 1;
                return i == ++i;
            }
            """,
            Call([], 1, b"", b"")
        )

    def test_order_lowerequal(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 1;
                return i <= i++;
            }
            """,
            Call([], 0, b"", b"")
        )
