# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import textwrap
import unittest

from .base import CompilerTestCase, Call


class TestCall(CompilerTestCase):

    maxDiff = None

    # def test_call_many_more_ops(self):
    #     self._test_program(
    #         """
    #         int mod(int a, int b) {
    #           printf("mod(a = %lli, b = %lli)\\n", a, b);
    #           if ((0 <= a) && (a < b)) return a;
    #           else if (a < 0) while ((a = a + b) < 0);
    #           else while ((a = a - b) >= b);
    #           return a;
    #         }
    #
    #         int segfault(int x1, int x2, int x3, int x4, int x5, int x6, int x7, int x8) {
    #             return x1;
    #         }
    #
    #         int main() {
    #               int x1, x2, x3, x4, x5, x6, x7, x8;
    #               int* t;
    #
    #               x1 = 2; x2 = 3; x3 = 2; x4 = 6; x5 = 6; x6 = 5; x7 = 12; x8 = -1;
    #               t = malloc(8);
    #               t[0] = 0;
    #
    #               //y1++;
    #               //foo = ++t[mod(z, 5)];
    #
    #               return ++t[mod(segfault(x1 - 1, x2, x3 - ((2*x1 + x2 + x3)%3) - 1, x4 - ((2*x8 + x4 + x6)%3), x5, x6 - ((x1 + 2*x2 + x3)%3) - 1, x7 + x5, x8 - 1), 1)];
    #         }
    #         """,
    #         Call([], 1, b"", b"")
    #     )

    # def test_call_many_more_ops(self):
    #     self._test_program(
    #         """
    #         int segfault(int x1, int x2, int x3, int x4, int x5, int x6, int x7, int x8) {
    #             return x1;
    #         }
    #
    #         int main() {
    #               int x1, x2, x3, x4, x5, x6, x7, x8;
    #               int* t;
    #               x1 = 2; x2 = 3; x3 = 2; x4 = 6; x5 = 6; x6 = 5; x7 = 12; x8 = -1;
    #
    #               return segfault(x1%8, x2%8, x3%8, x4%8, x5%8, x6%8, x7%8, x8%8);
    #         }
    #         """,
    #         Call([], 2, b"", b"")
    #     )
    #
    #


    def test_call_many_ops(self):
        self._test_program(
            """
            int show(int x, int y, int z) {
                printf("%i, %i, %i\\n", x, y, z);
            }

            int main() {
                int x1, x2, x3;
                x1 = x2 = x3 = 20;
                show(x1 - ((7*x1 + 3*x2 + 5*x3)%2) - 1, x2 + 2, x3);
                return 0;
            }

            """,
            Call([], 0, b"19, 22, 20\n", b"")
        )

    def test_call_on_return(self):
        self._test_program(
            """
            int inc(int x) {
                return x+1;
            }

            int main() {
                int i;
                i = 1;
                return inc(i);
            }
            """,
            Call([], 2, b"", b""),
        )

    def test_call_in_var(self):
        self._test_program(
            """
            int inc(int x) {
                return x+1;
            }

            int main() {
                int i, j;
                i = 1;
                j = inc(i);
                return j;
            }
            """,
            Call([], 2, b"", b""),
        )

    def test_two_args(self):
        self._test_program(
            """
            int add(int x, int y) {
                return x+y;
            }

            int main() {
                return add(add(1+1, 2), 3);
            }
            """,
            Call([], 7, b"", b"")
        )

    def test_three_args(self):
        self._test_program(
             """
             int add(int x, int y, int z) {
                 return x+y+z;
             }

             int main() {
                 return add(add(1,1,1), add(2,2,2), 3);
             }
             """,
             Call([], 12, b"", b""),
        )

    def test_argc_argv(self):
        self._test_program(
            """
            int main(int argc, char** argv) {
                printf(argv[1]);
                return argc;
            }
            """,
            Call(["Hello, world!"], 2, b"Hello, world!", b""),
            Call(["Hiiii", "there!"], 3, b"Hiiii", b"")
        )

    def test_argument_order_even(self):
        # Verifie que les arguments récupérés sur la pile
        # le sont dans le bon ordre
        self._test_program(
            """
            int fun(int a, int b, int c, int d, int e, int f, int g, int h, int i) {
                printf("%i%i%i%i%i%i%i%i%i", a, b, c, d, e, f, g, h, i);
            }

            int main() {
                fun(1, 2, 3, 4, 5, 6, 7, 8, 9);
                return 0;
            }
            """,
            Call([], 0, b"123456789", b""),
        )

    def test_argument_order_odd(self):
        # Verifie que les arguments récupérés sur la pile
        # le sont dans le bon ordre
        self._test_program(
            """
            int fun(int a, int b, int c, int d, int e, int f, int g, int h) {
                printf("%i%i%i%i%i%i%i%i", a, b, c, d, e, f, g, h);
            }

            int main() {
                fun(1, 2, 3, 4, 5, 6, 7, 8);
                return 0;
            }
            """,
            Call([], 0, b"12345678", b""),
        )

    def test_argument_order_even_libc(self):
        # Verifie que les arguments passés sur la pile
        # le sont dans le bon ordre
        self._test_program(
            """
            int main() {
                 printf("%i%i%i%i%i%i%i%i%i", 1, 2, 3, 4, 5, 6, 7, 8, 9);
                 return 0;
            }
            """,
            Call([], 0, b"123456789", b""),
        )

    def test_argument_order_odd_libc(self):
        # Verifie que les arguments passés sur la pile
        # le sont dans le bon ordre
        self._test_program(
            """
            int main() {
                 printf("%i%i%i%i%i%i%i%i", 1, 2, 3, 4, 5, 6, 7, 8);
                 return 0;
            }
            """,
            Call([], 0, b"12345678", b""),
        )

    def test_many_nested_calls(self):
        self._test_program(
            """
            int add(int a, int b, int c) {
              return a+b+c;
            }
            int main() {
              return add(add(add(add(1,2,3), add(1,2,3), add(1,2,3)),
                             add(add(1,2,3), add(1,2,3), add(1,2,3)),
                             add(add(1,2,3), add(1,2,3), add(1,2,3))),
                         add(add(add(1,2,3), add(1,2,3), add(1,2,3)),
                             add(add(1,2,3), add(1,2,3), add(1,2,3)),
                             add(add(1,2,3), add(1,2,3), add(1,2,3))),
                         add(add(add(1,2,3), add(1,2,3), add(1,2,3)),
                             add(add(1,2,3), add(1,2,3), add(1,2,3)),
                             add(add(1,2,3), add(1,2,3), add(1,2,3))));
            }
            """,
            Call([], 162, b"", b"")
        )




class TestRecursive(CompilerTestCase):

    def test_factorial(self):
        self._test_program(
            """
            int fact(int x) {
                return (x?x*fact(x-1):1);
            }

            int main(int argc, char** argv) {
                int x;
                x = atoi(argv[1]);
                printf("Fact %llu:\\n", x);
                printf("%llu\\n", fact(x));
                return 0;
            }

            """,
            Call(["1"], 0, b"Fact 1:\n1\n", b""),
            Call(["2"], 0, b"Fact 2:\n2\n", b""),
            Call(["3"], 0, b"Fact 3:\n6\n", b""),
            Call(["12"], 0, b"Fact 12:\n479001600\n", b""),
        )

    def test_factorial_tailrec(self):
        self._test_program(
            """
            int _fact(int x, int f) {
                return x?_fact(x-1, x*f):f;
            }

            int fact(int x) {
                return _fact(x, 1);
            }

            int main(int argc, char** argv) {
                int x;
                x = atoi(argv[1]);
                printf("Fact %llu:\\n", x);
                printf("%llu\\n", fact(x));
                return 0;
            }

            """,
            Call(["1"], 0, b"Fact 1:\n1\n", b""),
            Call(["2"], 0, b"Fact 2:\n2\n", b""),
            Call(["3"], 0, b"Fact 3:\n6\n", b""),
            Call(["12"], 0, b"Fact 12:\n479001600\n", b""),
        )

    def test_recursive_exceptions(self):
        self._test_program(
            """

            int cpt;

            int rec_counter(int i) {
                if (!cpt--) throw IsZero(i);
                return rec_counter(i+1);
            }

            int main(int argc, char** argv) {
                int x;
                x = atoi(argv[1]);

                cpt = x;

                try {
                    rec_counter(0);
                } catch (IsZero x) {
                    printf("cpt=0 after %i calls\\n", x);
                }

                return 0;
            }

            """,
            Call(["10000"], 0, b"cpt=0 after 10000 calls\n", b""),
        )
