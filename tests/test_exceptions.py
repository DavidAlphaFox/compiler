# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import textwrap
import unittest

from .base import CompilerTestCase, Call


class TestExceptions(CompilerTestCase):

    maxDiff = None

    def test_return_in_nested_tries_with_finally(self):
        self._test_program(
            """
            int main() {
                try {
                    try {
                        return 0;
                    } finally {
                        printf("F1\\n");
                    }
                } finally {
                    printf("F2\\n");
                }
                printf("F3\\n");
            }
            """,
            Call([], 0, b"F1\nF2\n", b"")
        )

    def test_return_in_nested_tries_with_return_in_parent_finally(self):
        self._test_program(
            """
            int main() {
                try {
                    try {
                        return 0;
                    } finally {
                        printf("F1\\n");
                    }
                } finally {
                    printf("F2\\n");
                    return 2;
                }
            }
            """,
            Call([], 2, b"F1\nF2\n", b"")
        )

    def test_return_in_nested_tries_with_return_in_child_finally(self):
        self._test_program(
            """
            int main() {
                try {
                    try {
                        return 0;
                    } finally {
                        printf("F1\\n");
                        return 1;
                    }
                } finally {
                    printf("F2\\n");
                }
            }
            """,
            Call([], 1, b"F1\nF2\n", b"")
        )

    def test_nested_tries_uncatched_exception(self):
        self._test_program(
            """
            int main() {
                try {
                    printf("T2\\n");
                    try {
                        printf("T1\\n");
                        throw B(1);
                    } catch(A n) {
                        printf("C1\\n");
                    } finally {
                        printf("F1\\n");
                    }
                } catch(B n) {
                    printf("C2\\n");
                } finally {
                    printf("F2\\n");
                }
                return 0;
            }
            """,
            Call([], 0, b"T2\nT1\nF1\nC2\nF2\n", b""),
        )

    def test_nested_tries_catched_exception(self):
        self._test_program(
            """
            int main() {
                try {
                    printf("T2\\n");
                    try {
                        printf("T1\\n");
                        throw A(1);
                    } catch(A n) {
                        printf("C1\\n");
                    } finally {
                        printf("F1\\n");
                    }
                } catch(A n) {
                    printf("C2\\n");
                } finally {
                    printf("F2\\n");
                }
                return 0;
            }
            """,
            Call([], 0, b"T2\nT1\nC1\nF1\nF2\n", b""),
        )

    def test_no_throw_in_try(self):
        self._test_program(
            """
            int main() {
                int n;
                try {
                    n = 1;
                } catch (A n) { n = 2;}

                return n;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_var_in_catch(self):
        self._test_program(
            """
            int main() {
                int n;
                n = 1;
                try {
                  throw A(3);
                } catch (A n) { return n;}
            }
            """,
            Call([], 3, b"", b""),
        )

    def test_uncatched(self):
        self._test_program(
            """
            int main() {
                throw Zut(13);
            }
            """,
            Call([], 1, b"", b"")
        )

    def test_two_handlers(self):
        self._test_program(
            """
            int main() {
                try {
                    throw B("");
                }
                catch (B x) { return 1; }
                catch (A x) { return 2; }
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_return_before_finally(self):
        self._test_program(
            """
            int main() {
               try {
                   throw A(2);
               } catch (A x) {
                   return 1+x;
               } finally {
                   return 0;
               }
            }
            """,
            Call([], 3, b"", b""),
        )

    def test_return_before_finally_reverse(self):
        self._test_program(
            """
            int main() {
               try {
                   throw A(2);
               } catch (A x) {
                   //printf("%i", x);
                   return x+1;
               } finally {
                   return 0;
               }
            }
            """,
            Call([], 3, b"", b""),
        )

    def test_return_in_block(self):
        self._test_program(
            """
            int main() {
                try {
                    return 1;
                } finally {
                    printf("Hello!\\n");
                }
           }
           """,
           Call([], 1, b"Hello!\n", b""),
       )

    def test_return_in_block_and_finally(self):
        self._test_program(
            """
            int main() {
                try {
                    return 1;
                } finally {
                    return 0;
                }
           }
           """,
           Call([], 0, b"", b""),
       )

    def test_return_in_finally(self):
        self._test_program(
            """
            int main() {
               try {
                   throw Z(2);
               } finally {
                   return 0;
               }
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_uncatched_exception(self):
        self._test_program(
            """
            int main() {
               int x;
               x = 0;
               try {
                  throw Z(2);
               } finally {
                  x++;
               }
               return x;
            }
            """,
            Call([], 1, b"", b"")
        )

    def test_function_return_in_try(self):
        self._test_program(
            """
            int* f(int* p) {
                try {
                    p[0]++;
                    return p;
                } finally {
                    p[0]++;
                }
            }

            int main() {
                int* x;
                x = malloc(8);
                x[0] = 0;
                return f(x)[0];
            }
            """,
            Call([], 2, b"", b""),
        )

    def test_var_in_catch_across_calls(self):
        self._test_program(
            """
            int rule_31() {
                int i; i = 5;
                throw E(42);
                printf("ERROR !\\n");
            }

            int main() {
                try{
                    int i; i=3;
                    rule_31();
                    printf("ERROR !\\n");
                } catch(E x) {
                    printf("%i\\n", x);
                }

                return 0;
            }
            """,
            Call([], 0, b"42\n", b""),
        )

    def test_var_in_catch_across_calls_many_locals(self):
        self._test_program(
            """
            int rule_31() {
                int i, j, k, l, m, n, o, p, q, r;
                i = 15; j = 14; k = 13;
                throw E(i+j+k);
                printf("ERROR !\n");
            }

            int main() {
                try{
                    int i, j, k;
                    i=3; j=2; k=1;
                    rule_31();
                    printf("ERROR !\n");
                } catch(E x) {
                    printf("%i\n", x);
                }

                return 0;
            }
            """,
            Call([], 0, b"42", b""),
        )

    def test_return_in_body_globals(self):
        self._test_program(
            """
            int i;

            int rule_33 () {
                try { return 0; } finally { i=99; }
            }

            int main() {
                i = 1;
                rule_33();
                printf("%i", i);
                return 0;
            }
            """,
            Call([], 0, b"99", b""),
        )

    def test_return_except_on_exception(self):
        self._test_program(
            """
            int f(int x) {
                try {
                    if (x==1) throw A(1);
                    return 0;
                    printf("NOPE\\n");
                } catch (A n) {
                    printf("HEY\\n");
                } finally {
                    printf("HI\\n");
                }
                printf("YES\\n");
                return 1;
            }

            int main(int argc, char** argv) {
                printf("%i\\n", argc);
                return f(argc);
            }
            """,
            Call([], 1, b"1\nHEY\nHI\nYES\n", b""),
            Call([0], 0, b"2\nHI\n", b""),
        )

    def test_catched_exceptions_in_finally(self):
        self._test_program(
            """
            int main() {
                try {
                    try {
                        throw A(4);
                    } finally {
                        try {
                            throw B(2);
                        } catch (B n) {
                            printf("%i\\n", n);
                        }
                    }
                } catch (A n) {
                    printf("%i\\n", n);
                    return n;
                }
                return 0;
            }
            """,
            Call([], 4, b"2\n4\n", b""),
        )

    def test_catched_exceptions_in_catch(self):
        self._test_program(
            """
            int main() {
                try {
                    throw A(1);
                } catch (A x) {
                    try {
                        throw B(2);
                    } catch (B n) {
                        printf("%i\\n%i\\n", n, x);
                    }
                }
                return 0;
            }
            """,
            Call([], 0, b"2\n1\n", b""),
        )

    def test_return_in_inner_try_outer_finally(self):
        self._test_program(
            """
            int main() {
              try {
                try {
                  return 1;
                }
                finally {}
              }
              finally {
                return 0;
              }
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_return_in_inner_catch_outer_finally(self):
        self._test_program(
            """
            int main() {
              try {
                try {
                  throw A(1);
                }
                catch (A n) {
                  return n;
                }
              }
              finally {
                return 0;
              }
              return 1;
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_throw_in_finally(self):
        self._test_program(
            """
            int main() {
                try {
                    try {} finally {
                      throw A(1);
                    }
                } catch (A n) {
                        return 0;
                }
                return 1;
            }
            """,
            Call([], 0, b"", b"")
        )

    def test_chain_throw_same_value(self):
        self._test_program(
            """
            int main() {
              try {
                try {
                  throw A(1);
                }
                catch (A n) {
                  throw B(n);
                }
              }
              catch (B n) {
                return 0;
              }
              return 1;
            }
            """
        )

    def test_reraise_after_finally(self):
        self._test_program(
            """
            int main() {
              try {
                try {
                  throw A(1);
                }
                finally {}
              }
              catch (A n) {
                return 0;
              }
              return 1;
            }
            """,
            Call([], 0, b"", b"")
        )
