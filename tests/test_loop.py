# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestWhile(CompilerTestCase):

    def test_while(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 3;

                while (i) {
                    printf("i=%d\\n", i--);
                };

                return 0;
            }
            """,
            Call([], 0, b"i=3\ni=2\ni=1\n", b""),
        )


class TestFor(CompilerTestCase):

    def test_for(self):
        self._test_program(
            """
            int main(int argc, char** argv) {
                int i;
                for (i=1; ++i < argc; i) {
                    printf("%s", argv[i]);
                }
                return 0;
            }
            """,
            Call(["1", "2", "3"], 0, b"23", b"")
        )

    def test_for_no_body(self):
        self._test_program(
            """
            int main() {
                int i;
                for (i=0; i < 10; printf("%i", i++));
                return 0;
            }
            """,
            Call([], 0, b"0123456789", b"")
        )
