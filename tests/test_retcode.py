# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestReturn(CompilerTestCase):

    def test_return_zero(self):
        self._test_program(
            """
            int main() {
                return 0;
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_return_one(self):
        self._test_program(
            """
            int main() {
                return 1;
            }
            """,
            Call([], 1, b"", b""),
        )
