# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from . import config
from .base import CompilerTestCase, Call


class TestUnOp(CompilerTestCase):

    def test_incr_2d_arrays(self):
        self._test_program(
            """
            int main() {
                int i, j;
                int x;
                i = malloc(8);
                j = i[0] = malloc(8);
                j[0] = 1;
                x = i[0][0]++;
                printf("%i", i[0][0]);
                return x;
            }
            """,
            Call([], 1, b"2", b"")
        )

    def test_incr_3d_arrays(self):
        self._test_program(
            """
            int main() {
                int i, j, k;
                int x;

                i = malloc(8);
                j = i[0] = malloc(8);
                k = j[0] = malloc(8);
                k[0] = 1;

                x = i[0][0][0]++;
                printf("%i", x);

                return i[0][0][0];
            }
            """,
            Call([], 2, b"1", b"")
        )

    def test_postinc_var(self):
        self._test_program(
            """
            int main() {
                int i, x;
                i = 1;
                x = i++;
                return x+i;
            }
            """,
            Call([], 3, b"", b""),
        )

    def test_preinc_var(self):
        self._test_program(
            """
            int main() {
                int i, x;
                i = 1;
                x = ++i;
                return x+i;
            }
            """,
            Call([], 4, b"", b""),
        )

    def test_postinc_const(self):
        self._test_program(
            """
            int i;

            int main() {
                int x;
                i = 1;

                x = i++;

                return x;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_preinc_const(self):
        self._test_program(
            """
            int i;

            int main() {
                int x;
                i = 1;

                x = ++i;

                return x;
            }
            """,
            Call([], 2, b"", b""),
        )

    def test_preinc_array(self):
        self._test_program(
            """
            int main() {
                int y;
                int* x;
                x = malloc(8);
                x[0] = 1;
                y = ++x[0];
                return y+x[0];
            }
            """,
            Call([], 4, b"", b""),
        )

    def test_postinc_array(self):
        self._test_program(
            """
            int main() {
                int y;
                int* x;
                x = malloc(8);
                x[0] = 1;
                y = x[0]++;
                return y+x[0];
            }
            """,
            Call([], 3, b"", b""),
        )

    def test_negation(self):
        self._test_program(
            """
            int main() {
                int i, x, y;
                i = 5;

                x = ~i;
                y = -i;

                printf("%lli\\n", x);
                printf("%lli\\n", y);

                return x == y-1;
            }
            """,
            Call([], 1, b"-6\n-5\n", b""),
        )

    def test_minus(self):
        self._test_program(
            """
            int main() {
                int i;
                i = -5;

                return -i;
            }
            """,
            Call([], 5, b"", b""),
        )

    def test_postinc_expr(self):
        self._test_program(
            """
            int main() {
                int t1, t2, i;


                i = 0;

                t1 = malloc(8);
                t2 = malloc(8);

                t1[0] = 1;
                t2[0] = 5;

                i = (i?t2:t1)[i++]++;

                printf("t1: [%d]\\nt2: [%d]", t1[0], t2[0]);

                return i;
            }
            """,
            Call([], 5, b"t1: [1]\nt2: [6]", b"")
        )

    def test_preinc_expr(self):
        self._test_program(
            """
            int main() {
                int t1, t2, i;


                i = 0;

                t1 = malloc(8);
                t2 = malloc(8);

                t1[0] = 1;
                t2[0] = 5;

                /* i++ is done before the eif, so in the end
                 * this is equivalent to i = ++t2[0].
                 */
                i = ++(i?t2:t1)[i++];

                printf("t1: [%d]\\nt2: [%d]", t1[0], t2[0]);

                return i;
            }
            """,
            Call([], 6, b"t1: [1]\nt2: [6]", b"")
        )
