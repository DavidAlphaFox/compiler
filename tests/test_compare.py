# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestCompare(CompilerTestCase):

    def test_eq_true(self):
        self._test_program(
            """
            int main() {
                return 1==1;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_eq_false(self):
        self._test_program(
            """
            int main() {
                return 1==2;
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_le_true_eq(self):
        self._test_program(
            """
            int main() {
                return 2<=2;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_le_true_lower(self):
        self._test_program(
            """
            int main() {
                return 1<=2;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_le_false(self):
        self._test_program(
            """
            int main() {
                return 3<=2;
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_lt_true(self):
        self._test_program(
            """
            int main() {
                return 1<12;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_lt_false(self):
        self._test_program(
            """
            int main() {
                return 13<12;
            }
            """,
            Call([], 0, b"", b""),
        )
