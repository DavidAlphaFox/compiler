# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import unittest
import textwrap

from .base import CompilerTestCase, Call


class TestExamples(CompilerTestCase):

    def test_order(self):
        self._test_program(
            self.projfs.gettext('Exemples/ordre.c'),
            Call([], 0, b"Valeur de j=7 (normalement 7), valeur de i=3.\n", b"")
        )

    def test_fact(self):
        self._test_program(
            self.projfs.gettext('Exemples/fact.c'),
            Call([], 10, b"", b"Usage: ./fact <n>\ncalcule et affiche la factorielle de <n>.\n"),
            Call([3], 0, b"La factorielle de 3 vaut 6 (en tout cas, modulo 2^32...).\n", b""),
            Call([-2], 10, b"", b"Ah non, quand meme, un nombre positif ou nul, s'il-vous-plait...\n")
        )

    def test_cat(self):
        self.testfs.settext("hello.txt", "Hello, World!\n")
        self.testfs.settext("hello2.txt", "I love computer science!\n")
        self._test_program(
            self.projfs.gettext('Exemples/cat.c'),
            Call([self.testfs.getsyspath("hello.txt")], 0, b"Hello, World!\n", b""),
            Call([self.testfs.getsyspath("hello.txt")]*2, 0, b"Hello, World!\n"*2, b""),
            Call(
                [self.testfs.getsyspath("hello.txt"),
                 self.testfs.getsyspath("hello2.txt")],
                 0, b"Hello, World!\nI love computer science!\n", b""
            ),
        )

    def test_sieve(self):
        exp = "Les nombres premiers inferieurs a 12 sont:\n{: 10} {: 10} {: 10} {: 10}\n{: 10} "
        self._test_program(
            self.projfs.gettext('Exemples/sieve.c'),
            Call([], 10, b"", b"Usage: ./sieve <n>\ncalcule et affiche les nombres premiers inferieurs a <n>.\n"),
            Call(["1"], 10, b"", b"Ah non, quand meme, un nombre >=2, s'il-vous-plait...\n"),
            Call(["12"], 0, exp.format(2, 3, 5, 7, 11).strip('\n').encode('utf-8'), b""),
        )


class TestExternal(CompilerTestCase):

    def test_unitest(self):
        self._test_program(
            self.projfs.gettext('tests/ext/unitest.c'),
            Call([], 0, self.projfs.getbytes('tests/ext/unitest.success.txt'), b'')
        )

    def test_unitexc(self):
        self._test_program(
            self.projfs.gettext('tests/ext/unitexc.c'),
            Call([], 1, self.projfs.getbytes('tests/ext/unitexc.success.txt'), b'')
        )
